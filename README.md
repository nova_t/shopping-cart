# Shopping Cart

A simple shopping cart API intended to checkout 4 (or more) items implemented in Ruby.

## Usage

### Run with Docker

#### Run tests

`./auto/test.sh`

#### Run app

`./auto/run.sh`

### Run locally

#### Setup

[`asdf`](https://asdf-vm.com/#/) is used to install ruby.

Add ruby plugin: `asdf plugin-add ruby https://github.com/asdf-vm/asdf-ruby.git`.

Then run `asdf install`

#### Run tests

`make test`

#### Run app

`make run`

## Design

`shopping-cart` has a two restful endpoints which either creates or updates a Cart with items.

| Endpoint           | Description                                                 |
|--------------------|-------------------------------------------------------------|
| POST /carts        | Create a cart with or without items                         |
| PATCH /carts/:id   | Add items to an existing cart                               |

Once a Cart model is created, it is stored as an in memory object in the cart repository. The same applies to Products and Promotions.
For the purpose of this exercise, each repositories only store objects in memory as opposed to more enduring options e.g. database.

#### Price calculation

Currently you can create 3 types of promotions. Freebie, Partial and Discount which represents the promotions in the requirement. Each promotion has an inner class Applier that applies the promotion
logic on to a collection of items. Which can adjust the price of existing item, add new item to the collection or both. The logics are detailed in each promotion spec.

See [freebie_spec.rb](./spec/models/promotions/freebie_spec.rb), [discount_spec.rb](./spec/models/promotions/discount_spec.rb), [partial_spec.rb](./spec/models/promotions/partial_spec.rb)


### Know restrictions and issues

- Given time constraints, I did not implement validation messages. Which means the endpoints will assume certain behaviour, for instance, when stock isn't available, cart will silently skips the item.
- It is unclear what the behaviour is when multiple promotions are effective on one single product, therefore the system can only handle one promotion per product.

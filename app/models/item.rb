module Models
  Item = Struct.new(:sku, :name, :price, :quantity) do
    class << self
      def from_product(product, quantity:, price: nil)
        actual_quantity = product.inventory > quantity ? quantity : product.inventory
        actual_price = price.nil? ? product.price : price
        new(product.sku, product.name, actual_price, actual_quantity)
      end

      def from_sku(sku, quantity:, price: nil)
        product = Repositories::Product.new.get(sku)

        actual_quantity = product.inventory > quantity ? quantity : product.inventory
        actual_price = price.nil? ? product.price : price
        new(product.sku, product.name, actual_price, actual_quantity)
      end
    end

    def as_json
      {
        sku: sku,
        name: name,
        price: price,
        quantity: quantity
      }
    end
  end
end

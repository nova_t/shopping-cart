module Models
  Product = Struct.new(:sku, :name, :price, :inventory)
end

require 'json'

module Models
  class Cart
    attr_accessor :id, :items

    def initialize(items: [])
      self.items = items
    end

    def add_item(item)
      self.items << item
    end

    def price
      promotions_applied_items.reduce(0) do |sum, item|
        sum += item.price * item.quantity
      end.round(2)
    end

    def as_json
      {
        id: id,
        items: promotions_applied_items.map(&:as_json),
        price: price
      }
    end

    private

    def promotions_applied_items
      @promotions_applied_items ||= apply_promotions
    end

    def apply_promotions
      promotions_applied_items = items.clone
      promotion_repo.list.each do |promotion|
        promotion.applied_to(promotions_applied_items)
      end
      @promotions_applied_items = promotions_applied_items
    end

    def promotion_repo
      Repositories::Promotion.new
    end
  end
end

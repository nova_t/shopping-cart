module Models
  module Promotions
    class Discount
      attr_reader :sku, :minimum_quantity, :discount

      def initialize(sku, minimum_quantity, discount)
        @sku = sku
        @minimum_quantity = minimum_quantity
        @discount = discount
      end

      def applied_to(items)
        Applier.new(self, items).apply
      end

      private

      class Applier < SimpleDelegator
        attr_reader :items

        def initialize(promotion, items)
          __setobj__(promotion)
          @items = items
        end

        def apply
          return items unless can_apply_promo?
          item_in_promo.price = (item_in_promo.price * (1 - discount)).round(2)
          items
        end

        private

        def can_apply_promo?
          !item_in_promo.nil? && item_in_promo.quantity >= minimum_quantity
        end

        def item_in_promo
          @item_in_promo ||= items.find { |item| item.sku == sku }
        end
      end
    end
  end
end

module Models
  module Promotions
    class Partial
      attr_reader :sku, :base_quantity, :priced_quantity

      def initialize(sku, base_quantity, priced_quantity)
        @sku = sku
        @base_quantity = base_quantity
        @priced_quantity = priced_quantity
      end

      def applied_to(items)
        Applier.new(self, items).apply
      end

      private

      class Applier < SimpleDelegator
        attr_reader :items

        def initialize(promotion, items)
          __setobj__(promotion)
          @items = items
        end

        def apply
          return items unless can_apply_promo?
          item_in_promo.quantity -= free_quantity * qualified_lots
          items << free_item
        end

        private

        def free_quantity
          base_quantity - priced_quantity
        end

        def free_item
          Item.from_sku(item_in_promo.sku, quantity: qualified_lots * free_quantity, price: 0)
        end

        def qualified_lots
          @lots ||= item_in_promo.quantity / base_quantity
        end

        def can_apply_promo?
          !item_in_promo.nil? && qualified_lots >= 1
        end

        def item_in_promo
          @item_in_promo ||= items.find { |item| item.sku == sku }
        end
      end
    end
  end
end


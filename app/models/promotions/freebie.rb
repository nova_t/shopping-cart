module Models
  module Promotions
    class Freebie
      attr_reader :sku, :free_item_sku

      def initialize(sku, free_item_sku)
        @sku = sku
        @free_item_sku = free_item_sku
      end

      def applied_to(items)
        Applier.new(self, items).apply
      end

      private

      class Applier < SimpleDelegator
        attr_reader :items

        def initialize(promotion, items)
          __setobj__(promotion)
          @items = items
        end

        def apply
          return items unless can_apply_promo?
          return items << free_item if free_item_not_in_cart?

          if in_cart_free_item.quantity > free_quantity
            in_cart_free_item.quantity -= free_quantity
            items << free_item
          else
            in_cart_free_item.quantity = free_quantity
            in_cart_free_item.price = 0
            items
          end
        end

        private

        def free_item
          Item.from_product(free_product, quantity: free_quantity, price: 0)
        end

        def in_cart_free_item
          @in_cart_free_item ||= items.find { |item| item.sku == free_item_sku }
        end

        def free_product
          product_repo.get(free_item_sku)
        end

        def free_item_not_in_cart?
          in_cart_free_item.nil?
        end

        def can_apply_promo?
          !item_in_promo.nil?
        end

        def item_in_promo
          @item_in_promo ||= items.find { |item| item.sku == sku }
        end

        def free_quantity
          item_in_promo.quantity
        end

        def product_repo
          Repositories::Product.new
        end
      end
    end
  end
end

module Repositories
  class Promotion
    @@in_memory_promotions = []

    def self.reset
      @@in_memory_promotions = []
    end

    def add(promotion)
      @@in_memory_promotions << promotion
    end

    def list
      @@in_memory_promotions
    end
  end
end

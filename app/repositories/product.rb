module Repositories
  class Product
    @@in_memory_products = Hash.new

    def self.reset
      @@in_memory_products = Hash.new
    end

    def self.in_memory_products
      @@in_memory_products
    end

    def self.in_memory_products=(in_memory_products)
      @@in_memory_products = in_memory_products
    end

    def get(sku)
      in_memory_products[sku]
    end

    private

    def in_memory_products
      self.class.in_memory_products
    end
  end
end

require 'securerandom'

module Repositories
  class Cart
    @@in_memory_carts = Hash.new

    def self.reset
      @@in_memory_carts = Hash.new
    end

    def self.in_memory_carts
      @@in_memory_carts
    end

    def self.in_memory_carts=(in_memory_carts)
      @@in_memory_carts = in_memory_carts
    end

    def count
      in_memory_carts.count
    end

    def save(cart)
      if cart.id.nil?
        cart.id = SecureRandom.hex
      end

      in_memory_carts[cart.id] = cart
    end

    def list
      in_memory_carts
    end

    def get(id)
      in_memory_carts[id]
    end

    private

    def in_memory_carts
      self.class.in_memory_carts
    end
  end
end

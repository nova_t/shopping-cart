require_relative '../models/cart'
require_relative '../repositories/cart'
require_relative '../repositories/product'

module Upserters
  Cart = Struct.new(:id, :item_params) do
    def commit
      item_params.each do |item_param|
        item_param.transform_keys!(&:to_sym)
        product = product_repository.get(item_param[:sku])

        if !product.nil? && product.inventory >= item_param[:quantity]
          cart.add_item(Models::Item.from_product(product, quantity: item_param[:quantity]))
        end
      end
      cart_repository.save(cart)
    end

    private

    def cart
      @cart ||= cart_repository.get(id) || Models::Cart.new
    end

    def cart_repository
      @cart_repo ||= Repositories::Cart.new
    end

    def product_repository
      @product_repo ||= Repositories::Product.new
    end
  end
end

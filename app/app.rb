require 'sinatra'
require 'json'

class ShoppingCart < Sinatra::Base

  set :strict_paths, false

  before do
    params.transform_keys(&:to_sym)
  end

  get '/' do
    'Hello Shopper'
  end

  post '/carts' do
    cart = cart_upserter.commit
    cart.as_json.to_json
  end

  patch '/carts/:id' do
    cart = cart_upserter.commit
    cart.as_json.to_json
  end

  private

  def cart_upserter
    Upserters::Cart.new(id, items)
  end

  def id
    params[:id]
  end

  def items
    body = request.body.read
    return [] if body.empty?
    JSON.parse(body)["items"] || []
  end
end

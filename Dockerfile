FROM ruby:2.7
MAINTAINER Nova Tan

WORKDIR /app
ADD Gemfile* /app/
RUN bundle install --jobs 8 --retry 3

ADD . /app

EXPOSE 8080
CMD ./bin/run.sh

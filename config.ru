require 'sinatra'
require './app/boot'

if ENV['RACK_ENV'] != 'test'
  product1 = Models::Product.new('120P90', 'Google Home', 49.99, 10)
  product2 = Models::Product.new('43N23P', 'MacBook Pro', 5399.99, 5)
  product3 = Models::Product.new('A304SD', 'Alexa Speaker', 109.50, 10)
  product4 = Models::Product.new('234234', 'Raspberry Pi B', 30, 2)

  Repositories::Product.in_memory_products = {
    product1.sku => product1,
    product2.sku => product2,
    product3.sku => product3,
    product4.sku => product4
  }

  promotion1 = Models::Promotions::Freebie.new('43N23P', '234234')
  promotion2 = Models::Promotions::Partial.new('120P90', 3, 2)
  promotion3 = Models::Promotions::Discount.new('A304SD', 3, 0.1)

  Repositories::Promotion.new.add(promotion1)
  Repositories::Promotion.new.add(promotion2)
  Repositories::Promotion.new.add(promotion3)
end

run ShoppingCart

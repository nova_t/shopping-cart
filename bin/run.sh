#!/bin/sh -eu

export UNICORN_TIMEOUT=${UNICORN_TIMEOUT:-10}

bundle exec unicorn -c config/unicorn.rb

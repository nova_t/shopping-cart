require_relative 'spec_helper'

describe 'The Shopping Cart App' do
  include Rack::Test::Methods

  def app
    ShoppingCart.new
  end

  let(:repository) { Repositories::Cart.new }

  describe 'POST /carts' do
    it 'returns 200' do
      post '/carts'
      expect(last_response).to be_ok
    end

    it 'creates a cart' do
      post '/carts'
      expect { post '/carts' }.to change{ repository.count }.by(1)
    end

    it 'returns newly created cart in json' do
      post '/carts'
      expected = repository.list.first[1].as_json.to_json

      expect(last_response.body).to eq expected
    end
  end

  describe 'PATCH /carts/:id' do
    let(:cart) { Models::Cart.new }
    let(:item_params) do
      [
        { sku: 'abc', quantity: 2 },
        { sku: 'def', quantity: 1 }
      ]
    end

    it 'returns 200' do
      repository.save(cart)
      patch "/carts/#{cart.id}", { items: item_params }.to_json

      expect(last_response).to be_ok
    end

    it 'does not create a cart' do
      repository.save(cart)
      expect { patch "/carts/#{cart.id}", items: item_params }.to change{ repository.count }.by(0)
    end
  end
end

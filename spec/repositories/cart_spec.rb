require_relative '../spec_helper'

describe Repositories::Cart do
  subject { described_class.new }

  describe '#count' do
    it 'relects the size of carts' do
      Repositories::Cart.in_memory_carts = {
        abc: Models::Cart.new,
        bcd: Models::Cart.new
      }

      expect(subject.count).to eq 2
    end
  end

  describe '#save' do
    let(:cart) { Models::Cart.new }

    it 'saves a new cart' do
      expect { subject.save(cart) }.to change{ subject.count }.by(1)
    end

    it 'generates an ID for the cart' do
      subject.save(cart)
      expect(cart.id).to_not be_nil
    end
  end

  describe '#list' do
    let(:cart) { Models::Cart.new }

    it 'returns all carts' do
      subject.save(cart)
      expected = { cart.id => cart }

      expect(subject.list).to eq expected
    end
  end

  describe '#get' do
    let(:cart) { Models::Cart.new }

    it 'returns a cart when cart exists' do
      subject.save(cart)
      expect(subject.get(cart.id)).to eq cart
    end

    it 'returns nil when cart does not exist' do
      expect(subject.get('non-existent-id')).to eq nil
    end
  end
end


require_relative '../spec_helper'

describe Repositories::Product do
  subject { described_class.new }

  describe '#get' do
    let(:product) { Models::Product.new(:abc) }

    it 'gets a product by sku' do
      Repositories::Product.in_memory_products = {
        abc: product,
        bcd: Models::Product.new(:bcd)
      }

      expect(subject.get(:abc)).to eq product
    end
  end
end

require 'rack/test'
require 'rspec'
require 'byebug'

require './app/boot'

RSpec.configure do |config|
  config.before(:each) do
    Repositories::Cart.reset
  end

  config.before(:each, data: :products) do
    product1 = Models::Product.new('aaa', 'product1', 10.99, 10)
    product2 = Models::Product.new('bbb', 'product2', 20, 5)
    product3 = Models::Product.new('ccc', 'product3', 109.50, 2)
    product4 = Models::Product.new('ddd', 'product4', 30, 2)

    Repositories::Product.in_memory_products = {
      product1.sku => product1,
      product2.sku => product2,
      product3.sku => product3,
      product4.sku => product4
    }
  end

  config.before(:each, data: :enough_products) do
    product1 = Models::Product.new('aaa', 'product1', 10.99, 100)
    product2 = Models::Product.new('bbb', 'product2', 20, 100)
    product3 = Models::Product.new('ccc', 'product3', 109.50, 100)
    product4 = Models::Product.new('ddd', 'product4', 30, 100)

    Repositories::Product.in_memory_products = {
      product1.sku => product1,
      product2.sku => product2,
      product3.sku => product3,
      product4.sku => product4
    }
  end

  config.after(:each, data: :products) do
    Repositories::Product.reset
  end

  config.after(:each, data: :enough_products) do
    Repositories::Product.reset
  end
end

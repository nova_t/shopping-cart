require_relative '../spec_helper'

describe Models::Cart, data: :enough_products do
  subject(:cart) { described_class.new(items: items) }
  let(:items) do
    [
      Models::Item.from_sku('aaa', quantity: 2, price: 10.99),
      Models::Item.from_sku('bbb', quantity: 3, price: 20),
      Models::Item.from_sku('ccc', quantity: 4, price: 109.50),
      Models::Item.from_sku('ddd', quantity: 12, price: 30)
    ]
  end

  describe '#price' do
    it 'adds up prices of every item without promitions' do
      expect(cart.price).to eq 879.98
    end

    context 'with promitions' do
      let(:promotion1)     { Models::Promotions::Freebie.new('aaa', 'bbb') }
      let(:promotion2)     { Models::Promotions::Discount.new('ccc', 3, 0.1) }
      let(:promotion3)     { Models::Promotions::Partial.new('ddd', 5, 3) }
      let(:promotion_repo) { Repositories::Promotion.new }

      it 'applies freebie promotion' do
        promotion_repo.add(promotion1)
        promotion_repo.add(promotion2)
        promotion_repo.add(promotion3)

        expect(cart.price).to eq 676.18

        Repositories::Promotion.reset
      end
    end
  end

  describe '#as_json' do
    let(:id) { 'abc' }
    let(:items) do
      [
        Models::Item.from_sku('aaa', quantity: 2, price: 10.99),
        Models::Item.from_sku('bbb', quantity: 3, price: 20)
      ]
    end

    let(:expected) do
      {
        id: 'abc',
        items: [
          {
            sku: 'aaa',
            name: 'product1',
            quantity: 2,
            price: 10.99
          },
          {
            sku: 'bbb',
            name: 'product2',
            quantity: 3,
            price: 20
          }
        ],
        price: 81.98
      }
    end

    it 'serialises the cart object' do
      cart.id = id
      expect(cart.as_json).to eq expected
    end
  end
end

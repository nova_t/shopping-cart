require_relative '../../spec_helper'

describe Models::Promotions::Discount, data: :products do
  subject(:promotion) { described_class.new('aaa', 3, 0.1) }
  let(:items)         { [Models::Item.from_sku('aaa', quantity: quantity, price: 10.99)] }

  describe '#applied_to' do
    subject { promotion.applied_to(items) }

    context 'when minimum quantity is met' do
      let(:quantity) { 3 }

      it 'applies discount' do
        expected = [Models::Item.from_sku('aaa', quantity: 3, price: 9.89)]
        expect(subject).to eq expected
      end
    end

    context 'when minimum quantity is not met' do
      let(:quantity) { 2 }

      it 'does not apply discount' do
        expect(subject).to eq items
      end
    end
  end
end

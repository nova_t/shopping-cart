require_relative '../../spec_helper'

describe Models::Promotions::Freebie, data: :products do
  subject(:promotion) { described_class.new('aaa', 'bbb') }

  describe '#applied_to' do
    subject { promotion.applied_to(items) }

    context 'when free product has enough stock' do
      let(:items) do
        [
          Models::Item.from_sku('aaa', quantity: 2),
          Models::Item.from_sku('ccc', quantity: 3)
        ]
      end

      it 'adds free items with price being 0' do
        expected = [
          Models::Item.from_sku('aaa', quantity: 2),
          Models::Item.from_sku('ccc', quantity: 3),
          Models::Item.from_sku('bbb', quantity: 2, price: 0)
        ]
        expect(subject).to eq expected
      end
    end

    context 'when free product does not have enough stock' do
      let(:items) do
        [
          Models::Item.from_sku('aaa', quantity: 10),
          Models::Item.from_sku('ccc', quantity: 3)
        ]
      end

      it 'adds as many free items as stock available' do
        expected = [
          Models::Item.from_sku('aaa', quantity: 10),
          Models::Item.from_sku('ccc', quantity: 3),
          Models::Item.from_sku('bbb', quantity: 5, price: 0)
        ]
        expect(subject).to eq expected
      end
    end

    context 'when free item is already in the cart and quantity is more than free quantity from promotion' do
      let(:items) do
        [
          Models::Item.from_sku('aaa', quantity: 2),
          Models::Item.from_sku('bbb', quantity: 3)
        ]
      end

      it 'adjust price of free item to 0' do
        expected = [
          Models::Item.from_sku('aaa', quantity: 2),
          Models::Item.from_sku('bbb', quantity: 1),
          Models::Item.from_sku('bbb', quantity: 2, price: 0)
        ]
        expect(subject).to eq expected
      end
    end

    context 'when free item is already in the cart and quantity is equal to free quantity from promotion' do
      let(:items) do
        [
          Models::Item.from_sku('aaa', quantity: 2),
          Models::Item.from_sku('bbb', quantity: 2)
        ]
      end

      it 'adjust price of free item to 0' do
        expected = [
          Models::Item.from_sku('aaa', quantity: 2),
          Models::Item.from_sku('bbb', quantity: 2, price: 0)
        ]
        expect(subject).to eq expected
      end
    end

    context 'when free item is already in the cart and quantity is less than free quantity from promotion' do
      let(:items) do
        [
          Models::Item.from_sku('aaa', quantity: 2),
          Models::Item.from_sku('bbb', quantity: 1)
        ]
      end

      it 'adjust price of free item to 0' do
        expected = [
          Models::Item.from_sku('aaa', quantity: 2),
          Models::Item.from_sku('bbb', quantity: 2, price: 0)
        ]
        expect(subject).to eq expected
      end
    end
  end
end

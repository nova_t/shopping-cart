require_relative '../../spec_helper'

describe Models::Promotions::Partial, data: :products do
  subject(:promotion) { described_class.new('aaa', 3, 2) }
  let(:items)         { [Models::Item.from_sku('aaa', quantity: quantity, price: 10.99)] }

  describe '#applied_to' do
    subject { promotion.applied_to(items) }

    context 'when base quantity is met' do
      let(:quantity) { 7 }

      it 'applies discount' do
        expected = [
          Models::Item.from_sku('aaa', quantity: 5, price: 10.99),
          Models::Item.from_sku('aaa', quantity: 2, price: 0)
        ]
        expect(subject).to eq expected
      end
    end

    context 'when base quantity is not met' do
      let(:quantity) { 2 }

      it 'does not apply discount' do
        expect(subject).to eq items
      end
    end
  end
end

require_relative '../spec_helper'

describe Upserters::Cart do
  subject { described_class.new(id, item_params) }

  let(:cart_repository) { Repositories::Cart.new }
  let(:item_params)     { [] }
  let(:id)              { nil }
  let(:product1)        { Models::Product.new('abc', 'product1', 10.99, 10) }
  let(:product2)        { Models::Product.new('def', 'product2', 20, 5) }
  let!(:products) do
    Repositories::Product.in_memory_products = {
      product1.sku => product1,
      product2.sku => product2
    }
  end

  describe '#commit' do
    context 'when id does not exist' do
      it 'creates a cart' do
        expect { subject.commit }.to change{ cart_repository.count }.by(1)
      end
    end

    context 'when cart already exists' do
      let(:id) { cart.id }
      let(:cart) do
        cart_repository.save(Models::Cart.new)
      end

      it 'deoes not create a cart' do
        subject.id = cart.id
        expect { subject.commit }.to change{ cart_repository.count }.by(0)
      end
    end

    context 'when stock is available' do
      let(:item_params) do
        [
          { sku: 'abc', quantity: 2 },
          { sku: 'def', quantity: 1 }
        ]
      end

      it 'assigns items to the cart' do
        cart = subject.commit
        expected = [
          Models::Item.new('abc', 'product1', 10.99, 2),
          Models::Item.new('def', 'product2', 20, 1)
        ]
        expect(cart.items).to eq expected
      end
    end

    context 'when stock is not available' do
      let(:item_params) do
        [
          { sku: 'abc', quantity: 2 },
          { sku: 'def', quantity: 6 }
        ]
      end

      it 'does not assign item' do
        cart = subject.commit
        expected = [Models::Item.new('abc', 'product1', 10.99, 2)]

        expect(cart.items).to eq expected
      end
    end
  end
end

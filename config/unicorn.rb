require 'rubygems'
require 'ougai'

working_directory File.expand_path("../../", __FILE__)

listen "8080"
timeout ENV['UNICORN_TIMEOUT'].to_i
logger Ougai::Logger.new(STDOUT)

before_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn master intercepting TERM and sending myself QUIT instead'
    Process.kill 'QUIT', Process.pid
  end
end

after_fork do |server, worker|
  Signal.trap 'TERM' do
    puts 'Unicorn worker intercepting TERM and doing nothing. Wait for master to send QUIT'
  end
end
